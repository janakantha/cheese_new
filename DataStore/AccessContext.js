
import API from '../api/WepAPI'
import myReducer from '../DataStore/MyReducer'
import Types from '../utils/aTypes'
import axios from "axios";

import appContext from './MyContext'



function IsJsonString(text) {
  if (text.includes("text/javascript")) {
    return false;
  }
  else {
    return true;
  }
}

const setUserData = (dispatch) => (username, password) => { //async () =>


  try {
    const response = API.get(`assets/gate/login.cfc?method=signin&returnformat=json&keep=1&username=${username}&password=${password}`);


    if (response.data !== undefined) {
      dispatch({ type: Types.SET_USERDATA, payload: response.data });
      console.log(response.data)
    } else {
      dispatch({ type: Types.SET_USERDATA, payload: response });
      console.log(response._W.data)
    }

  } catch (error) {
    console.log("error login", error);
    dispatch({ type: Types.ERROR, payload: [] });

  }

}

const fetchMenuDetails = (dispatch) => (Dish_Id) => {

  try {
    const response =  API.get(`/administrator/index.cfm?p=dishmenu/MenuDish&method=MenuDish&MenuDishId=${Dish_Id}`);


    dispatch({ type: Types.GET_MENUDETAILS, payload: response });

    console.log(response._W._W.data);

    // if (IsJsonString(response.data)) {
    //   dispatch({ type: Types.GET_MENUDETAILS, payload: response.data });
    //   console.log(JSON.stringify(response.data));
    // } else {
    //   dispatch({ type: Types.GET_MENUDETAILS, payload: [] });
    // }

  } catch {
    dispatch({ type: Types.ERROR, payload: [] });
  }
}


const fetchCategories = (dispatch) => async () => {
  console.log('fetchCategories')
  try {
    const response = await API.get("/administrator/index.cfm?p=dishmenu/Menu&method=list");

    if (IsJsonString(response.data)) {
      dispatch({ type: Types.ALL_CATEGORIES, payload: response.data });
    } else {
      dispatch({ type: Types.ALL_CATEGORIES, payload: [] });
    }

  } catch {
    dispatch({ type: Types.ERROR, payload: [] });
  }
};

const fetchFoodList = (dispatch) => async () => {
  try {
    const response = await API.get("/administrator/index.cfm?p=dishmenu/MenuDish&method=list");

    if (IsJsonString(response.data)) {
      dispatch({ type: Types.ALL_FOODS, payload: response.data });
    } else {
      dispatch({ type: Types.ALL_FOODS, payload: [] });
    }

  } catch {
    dispatch({ type: Types.ERROR, payload: [] });
  }
};

const fetchBanners = (dispatch) => async () => {
  try {

    let local =
      [
        "http://tutofox.com/foodapp//banner/banner-1.jpg",
        "http://tutofox.com/foodapp//banner/banner-2.jpg",
        "http://tutofox.com/foodapp//banner/banner-3.png"

      ];

    const response = await axios.get("http://tutofox.com/foodapp/api.json");

    let urls = response.data.banner;


    dispatch({ type: Types.GET_BANNERS, payload: urls });
    //dispatch({ type: Types.GET_BANNERS, payload: local });
  } catch {

    dispatch({ type: Types.ERROR, payload: "Data Not found" });
  }
};

export const { Provider, Context } = appContext(
  myReducer,
  {
    fetchCategories,
    fetchFoodList,
    fetchBanners,
    fetchMenuDetails,
    setUserData
  },
  { msg: null }
);