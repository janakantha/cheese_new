
import Types from '../utils/aTypes'


const myReducer = (state, action) => {

    switch (action.type) {
        case Types.ALL_CATEGORIES:
            return { ...state, dataCategories: action.payload };
        case Types.ALL_FOODS:
            return { ...state, dataFoods: action.payload };
        case Types.GET_BANNERS:
            return { ...state, banners: action.payload };
        case Types.SET_USERDATA:
            return { ...state, userdata: action.payload }
        case Types.GET_MENUDETAILS:
            return { ...state, menuDetails: action.payload }
        case Types.ERROR:
            return {
                ...state,
                msg: action.payload,
            };
        default:
            return state;
    }
};

export default myReducer;