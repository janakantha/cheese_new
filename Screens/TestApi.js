import React, { useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import axios from 'axios';


const TestApi = ({navigation}) => {

    const [logindata, setLoginData] = useState('');
    const [menudata, setMenuData] = useState('');

    function getLoginData() {

       
        try {
            var config = {
                method: 'get',
                url: `http://nellicafe.com/assets/gate/login.cfc?method=signin&returnformat=json&keep=1&username=Admin&password=Kiribath12`,
                headers: {
                    'Content-Type': 'application/json',
                    'App':'pos'
                }
            };

            axios(config).then(response => {
                console.log(JSON.stringify(response.data))
                setLoginData(JSON.stringify(response.data));
            });
        } catch (error) {
            console.log(error);
        }
    }


    function getMenuData() {


        try {
            var config = {
                method: 'get',
                url: `http://nellicafe.com/administrator/index.cfm?p=dishmenu/Menu&method=list`,

            };

            axios(config).then(response => {
                console.log(response.data)
                setMenuData(JSON.stringify(response.data));
            });
        } catch (error) {
            console.log(error);
        }
    }


    return (
        <View>

            <TouchableOpacity onPress={() => { getLoginData() }}>
                <Text style={{ fontSize: 30 }}>Check Login Data</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { getMenuData() }}>
                <Text style={{ fontSize: 30 }}>check Menu Data</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => { navigation.navigate('Test2') }}>
                <Text style={{ fontSize: 30 }}>Second screen</Text>
            </TouchableOpacity>

            <View>
                <Text>Login data ----------------</Text>
                <Text>{logindata}</Text>
                <Text>menu group data -------------------</Text>
                <Text>{menudata}</Text>
            </View>

        </View>
    )
}

export default TestApi
