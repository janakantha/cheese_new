import React, { useState,useEffect ,useContext } from 'react'
import { View, Text, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Input } from "react-native-elements";

import { Context as Mycontext } from '../DataStore/AccessContext';
import API from '../api/WepAPI'

const LoginScreen = ({ navigation }) => {

    const { state, setUserData } = useContext(Mycontext);

    const { userdata } = state;

    useEffect(() => {

        //setUserData("Admin","Kiribath12");

    }, []);


    const [username, setUserName] = useState('');
    const [password, setPassword] = useState('');

    return (
        <SafeAreaView style={styles.contentView} forceInset={{ top: 'always' }}>

            <View style={styles.titleView}>

                <Image resizeMode="contain" style={{ height: 70, width: 70 }} source={require('../assets/user_p.png')} />
            </View>
            <View style={styles.listView}>

                <View>
                    <View style={{ width: 350, alignSelf: 'center' }}>
                        <Input
                            placeholder="Your Email"
                           
                            autoCorrect={false}
                            onChangeText={(text) => { setUserName(text) }}
                        />
                        <Input
                            placeholder="Password"
                            //secureTextEntry
                            
                            autoCorrect={false}
                            onChangeText={(text) => { setPassword(text) }}
                        />
                        <View style={{ alignSelf: 'flex-end', marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => { navigation.navigate('Forgot'); }}>
                                <Text style={{ fontStyle: 'italic' }}>Forgot Password ?</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => {
                        setUserData(username, password);
                        alert(JSON.stringify(userdata));
                        //getLoginData() 
                    }}
                        style={{
                            width: 300,
                            height: 40,
                            backgroundColor: "#f15b5d",
                            alignSelf: "center",
                            borderRadius: 30,
                        }}>
                        <Text style={{
                            alignSelf: 'center',
                            fontSize: 20,
                            fontWeight: "600",
                            color: "#fff",
                            marginTop: 5
                        }}>Sign In Now</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        navigation.navigate('SignUp');
                    }} style={{
                        width: 300,
                        height: 40,
                        backgroundColor: "#fff",
                        alignSelf: "center",
                        borderRadius: 30,
                        marginVertical: 15
                    }}>
                        <Text style={{
                            alignSelf: 'center',
                            fontSize: 20,
                            fontWeight: "600",
                            color: "#000",
                            marginTop: 5
                        }}>Sign Up Now</Text>
                    </TouchableOpacity>

                </View>
                <View style={{ alignSelf: 'center', marginBottom: 10 }}>
                    <Text style={{ fontStyle: 'italic' }}>-- Or --</Text>
                </View>
                <View style={{ flexDirection: 'row', marginHorizontal: 140, justifyContent: 'space-between' }}>
                    <TouchableOpacity>
                        <Image style={{ height: 40, width: 40 }} resizeMode="contain" source={require('../assets/gplus.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image style={{ height: 40, width: 40 }} resizeMode="contain" source={require('../assets/fb.png')} />
                    </TouchableOpacity>
                </View>
            </View>

        </SafeAreaView>
    )
}

export default LoginScreen;

const styles = StyleSheet.create({
    contentView: {
        backgroundColor: '#F2F2F2',
        flex: 1,
        justifyContent: 'space-between',
    },
    titleView: {
        flex: 1,
        alignSelf: 'center',
        marginTop: 35
    },
    listView: {
        paddingTop: 15,
        flex: 6,
    },
    bottomView: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingBottom: 20,
    },
    txtInputView: {
        marginTop: 10,
        marginBottom: 10,
    },
    spacer: {
        margin: 10,
        marginTop: 20,
        marginBottom: 20,
    },
    titleStyle: {
        fontSize: 18,
        fontWeight: "400",
        color: "#f15b5d",
    },
});




