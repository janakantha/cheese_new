import React, { useContext, useEffect, useRef } from "react";
import { View, StyleSheet, Image, SafeAreaView, TouchableOpacity, Button, Text } from "react-native";

import CardView from '../components/CardView'

const CartScreen = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.contentView} forceInset={{ top: "always" }}>
            <View style={styles.titleView}>
                <Text style={{
                    fontSize: 30,
                    fontWeight: "600",
                    color: "#000",
                    marginTop: 5
                }} > My Cart</Text>
                <TouchableOpacity
                    style={{ alignItems: "center" }}
                    onPress={() => {
                        // navigate("Order");
                    }}
                >
                    <Image source={require('../assets/Fast_Cart.png')} style={styles.imgIcon} />
                </TouchableOpacity>
            </View>
            <View style={styles.listView}>

                {/* <CardView 
                title={'kottu'}
                category={'instant'}
                info={'dcdccd  dd'}
                price={'345'}
                /> */}

                <View style={styles.smallCard}>
                    <View style={styles.productInfo}>
                        <Text style={styles.title}>kottu with chicken curry</Text>
                        <Text style={styles.resturentTitle}>
                            Food Category
                        </Text>
                        <Text style={styles.foodDescription}>lolo lo l o </Text>
                    </View>
                    <View style={styles.priceView}>
                        <Text style={styles.price}>LKR 255</Text>
                        <View style={styles.countView}>
                            <TouchableOpacity style={{
                                width: 30,
                                height: 40,
                                backgroundColor: "#f15b5d",
                                alignSelf: "center",
                                borderRadius: 30,
                            }}>
                                <Text style={{
                                    alignSelf: 'center',
                                    fontSize: 20,
                                    fontWeight: "600",
                                    color: "#fff",
                                    marginTop: 5
                                }}>-</Text>
                            </TouchableOpacity>

                            <Text
                                h4
                                style={{ alignSelf: "center", margin: 5, fontWeight: "600" }}
                            >
                                #1
                            </Text>
                            <TouchableOpacity style={{
                                width: 30,
                                height: 40,
                                backgroundColor: "#f15b5d",
                                alignSelf: "center",
                                borderRadius: 30,
                            }}>
                                <Text style={{
                                    alignSelf: 'center',
                                    fontSize: 20,
                                    fontWeight: "600",
                                    color: "#fff",
                                    marginTop: 5
                                }}>+</Text>
                            </TouchableOpacity>

                        </View>
                    </View>

                </View>
                <View style={styles.smallCard}>
                    <View style={styles.productInfo}>
                        <Text style={styles.title}>Chicken fried Rice</Text>
                        <Text style={styles.resturentTitle}>
                            Food Category
                        </Text>
                        <Text style={styles.foodDescription}>ipsu,,,m </Text>
                    </View>
                    <View style={styles.priceView}>
                        <Text style={styles.price}>LKR 455</Text>
                        <View style={styles.countView}>
                            <TouchableOpacity style={{
                                width: 30,
                                height: 40,
                                backgroundColor: "#f15b5d",
                                alignSelf: "center",
                                borderRadius: 30,
                            }}>
                                <Text style={{
                                    alignSelf: 'center',
                                    fontSize: 20,
                                    fontWeight: "600",
                                    color: "#fff",
                                    marginTop: 5
                                }}>-</Text>
                            </TouchableOpacity>

                            <Text
                                h4
                                style={{ alignSelf: "center", margin: 5, fontWeight: "600" }}
                            >
                                #1
                            </Text>
                            <TouchableOpacity style={{
                                width: 30,
                                height: 40,
                                backgroundColor: "#f15b5d",
                                alignSelf: "center",
                                borderRadius: 30,
                            }}>
                                <Text style={{
                                    alignSelf: 'center',
                                    fontSize: 20,
                                    fontWeight: "600",
                                    color: "#fff",
                                    marginTop: 5
                                }}>+</Text>
                            </TouchableOpacity>

                        </View>
                    </View>

                </View>

                <View style={styles.bottomView}>
                    <View style={styles.amountDetails}>
                        <Text style={{ fontSize: 18 }}> Total</Text>
                        <Text style={{ fontSize: 18, fontWeight: "600" }}>
                            LKR 2345
                        </Text>
                    </View>
                    <TouchableOpacity onPress={() => {
                        navigation.navigate('Payment');
                    }} style={{
                        width: 300,
                        height: 40,
                        backgroundColor: "#f15b5d",
                        alignSelf: "center",
                        borderRadius: 30,
                    }}>
                        <Text style={{
                            alignSelf: 'center',
                            fontSize: 20,
                            fontWeight: "600",
                            color: "#fff",
                            marginTop: 5
                        }}>Order Now</Text>
                    </TouchableOpacity>

                </View>

            </View>

        </SafeAreaView>
    )
}

export default CartScreen


const styles = StyleSheet.create({
    contentView: {
        backgroundColor: "#F2F2F2",
        flex: 1,
        justifyContent: "space-between",
    },
    titleView: {
        flex: 1,
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "row",
        paddingLeft: 20,
        paddingRight: 20,
    },
    listView: {
        flex: 9,
    },
    bottomView: {
        flex: 2,
    },

    imgIcon: {
        width: 60,
        height: 60,
    },
    searchOptions: {
        display: "flex",
        height: 60,
        justifyContent: "space-around",
        flexDirection: "row",
        alignItems: "center",
        marginLeft: 10,
    },
    topCategory: {
        height: 100,
        backgroundColor: "green",
    },

    amountDetails: {
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 10,
        margin: 5,
    },
    options: {
        display: "flex",
        height: 80,
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "row",
        paddingLeft: 20,
        paddingRight: 20,
        borderTopColor: "#DFDFDF",
        borderTopWidth: 0.5,
        borderBottomColor: "#DFDFDF",
        borderBottomWidth: 0.5,
    },
    optionsText: {
        fontSize: 18,
    },
    icon: {
        width: 40,
        height: 40,
    },
    smallCard: {
        flex: 1,
        minHeight: 100,
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        backgroundColor: "white",
        borderColor: "#E5E5E5",
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        margin: 10,
    },

    title: {
        fontSize: 22,
        fontWeight: "300",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    resturentTitle: {
        fontSize: 16,
        fontWeight: "600",
        marginTop: 4,
        marginBottom: 4,
        display: "flex",
        color: "#565555",
    },
    foodDescription: {
        fontSize: 16,
        fontWeight: "300",
        display: "flex",
        color: "#565555",
    },
    price: {
        fontSize: 18,
        fontWeight: "600",
        display: "flex",
        color: "#EA5656",
        alignSelf: "center",
    },
    foodImageSmall: {
        borderRadius: 10,
        height: 99,
        width: 99,
        justifyContent: "flex-start",
        alignItems: "center",
        backgroundColor: "#581845",
        alignSelf: "center",
    },
    rating: {
        alignSelf: "flex-start",
    },
    productInfo: {
        flex: 9,
        justifyContent: "space-around",
    },

    priceView: {
        flex: 3,
        justifyContent: "space-around",
        alignItems: "flex-start",
    },
    countView: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        flex: 8,
    },
    badge: {
        backgroundColor: "green",
    },
    //Button
    btnAddRemove: {
        borderColor: "#f15b5d",
        borderRadius: 5,
        borderWidth: 0.5,
    },
    btnTitleStyle: {
        color: "#f15b5d",
    },
});
