import React from "react";
import { View, StyleSheet, Image, TouchableOpacity, Text, Button } from "react-native";
import { SafeAreaView } from "react-navigation";

import OrderCard from '../components/OrderCard'

const OrderScreen = ({ navigation }) => {



    const didSelect = ({ item }) => {
        onViewOrderDetails(item);
    };

    const onCancel = ({ item }) => {
        console.log(item);
    };

    const didTapBack = () => {
        navigation.goBack();
    };

    return (
        <SafeAreaView style={styles.contentView} forceInset={{ top: "always" }}>
            <View style={styles.titleView}>
                <TouchableOpacity onPress={() => didTapBack()}>
                    <Image style={styles.imgIcon} source={require('../assets/shopping_cart.png')} />
                </TouchableOpacity>
                <Text h4 style={{ flex: 1, textAlign: "center", marginRight: 40,fontSize:30 }}>
                    My Orders
        </Text>
            </View>

            <TouchableOpacity
                style={styles.smallCard}
                onPress={(data) => {}}
            >
                <View style={styles.productInfo}>
                    <View
                        style={{
                            flex: 6,
                            padding: 5,
                            paddingLeft: 20,
                        }}
                    >
                        <Text style={styles.title}>Order ID: 9932</Text>
                        <Text style={styles.orderDateTitle}>
                            2020 10 22
                        </Text>
                        <Text style={styles.price}>LKR 2232</Text>
                    </View>
                    <View
                        style={{
                            flex: 6,
                            justifyContent: 'space-around',
                            padding: 5,
                            flexDirection: 'row',
                        }}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                justifyContent: 'center',
                                fontSize: 16,
                                color: '#7FBB69',
                                fontWeight: '600',
                            }}
                        >
                            Delivered
        </Text>
                        <Image source={require('../assets/User.png')} style={styles.statusOnGoing} />
                    </View>
                </View>
            </TouchableOpacity>
            
            <OrderCard/>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    contentView: {
        backgroundColor: "#F2F2F2",
        flex: 1,
        justifyContent: "space-between",
    },
    
    titleView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        paddingLeft: 20,
        paddingRight: 20,
    },
    listView: {
        flex: 9,
    },
    bottomView: {
        flex: 2,
    },

    imgIcon: {
        width: 40,
        height: 50,
    },
    searchOptions: {
        display: "flex",
        height: 60,
        justifyContent: "space-around",
        flexDirection: "row",
        alignItems: "center",
        marginLeft: 10,
    },
    topCategory: {
        height: 100,
        backgroundColor: "green",
    },

    amountDetails: {
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 10,
        margin: 5,
    },
    smallCard: {
        flex: 1,
        height: 100,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        borderColor: '#E5E5E5',
        borderWidth: 1,
        borderRadius: 10,
        margin: 10,
    },

    title: {
        fontSize: 22,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    orderDateTitle: {
        marginTop: 5,
        fontSize: 18,
        fontWeight: '400',
        display: 'flex',
        color: '#565555',
    },
    price: {
        fontSize: 25,
        fontWeight: '600',
        display: 'flex',
        color: '#EA5656',
        marginTop: 5,
    },
    statusOnGoing: {
        height: 60,
        width: 60,
        justifyContent: 'flex-start',
        alignItems: 'center',
        alignSelf: 'center',
    },
    statusCompleted: {
        height: 80,
        width: 80,
        justifyContent: 'flex-start',
        alignItems: 'center',
        alignSelf: 'center',
    },

    rating: {
        alignSelf: 'flex-start',
    },
    productInfo: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems: 'center',
    },
});



export default OrderScreen;

