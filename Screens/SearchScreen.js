import React from "react";
import { View, StyleSheet, Text, Image, TouchableOpacity,TextInput } from "react-native";
import { SafeAreaView } from "react-navigation";

const SearchScreen = ({ navigation }) => {


    return (
        <SafeAreaView style={styles.contentView} forceInset={{ top: "always" }}>
            <View style={styles.titleView}>
                <View style={styles.searchOptions}>
                    <TouchableOpacity onPress={() =>{}}>
                        <Image style={styles.imgIcon} source={require('../assets/User.png')} />
                    </TouchableOpacity>
                    <View style={styles.root}>
                        <View style={styles.searchBar}>
                            <Image style={styles.searchIcon} source={require('../assets/search.png')} />
                            <TextInput
                                style={styles.searchTextField}
                                placeholderTextColor={"#939395"}
                                placeholder={"Search Foods"}
                               
                    
                            />
                        </View>
                    </View>
                </View>
            </View>
           
                <View style={styles.smallCard}>
                    <View style={styles.productInfo}>
                        <Text style={styles.title}>Chicken fried Rice</Text>
                        <Text style={styles.resturentTitle}>
                            Food Category
                        </Text>
                        
                    </View>
                    <View style={styles.priceView}>
                        <Text style={styles.price}>LKR 455</Text>
                        <View style={styles.countView}>

                        </View>
                    </View>


                </View>
                <View style={styles.smallCard}>
                    <View style={styles.productInfo}>
                        <Text style={styles.title}>Chicken fried Rice</Text>
                        <Text style={styles.resturentTitle}>
                            Food Category
                        </Text>
                        
                    </View>
                    <View style={styles.priceView}>
                        <Text style={styles.price}>LKR 455</Text>
                        <View style={styles.countView}>

                        </View>
                    </View>


                </View>
                
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    contentView: {
        backgroundColor: "#F2F2F2",
        flex: 1,
        justifyContent: "space-between",
    },
    titleView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    listView: {
        flex: 9,
    },
    imgIcon: {
        width: 40,
        height: 50,
    },
    searchOptions: {
        display: "flex",
        height: 60,
        justifyContent: "space-around",
        flexDirection: "row",
        alignItems: "center",
        marginLeft: 10,
    },
    topCategory: {
        height: 100,
        backgroundColor: "green",
    },
    root: {
        flex: 1,
        height: 60,
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignContent: "center",
        alignItems: "center",
        paddingLeft: 20,
        paddingRight: 20,
    },
    searchBar: {
        flex: 1,
        height: 42,
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: "#ededed",
        alignItems: "center",
        borderRadius: 20,
        paddingLeft: 10,
        paddingRight: 10,
        borderColor: "#E5E5E5",
        borderWidth: 2,
    },

    searchIcon: {
        width: 25,
        height: 25,
    },
    searchTextField: {
        marginLeft: 5,
        flex: 9,
        display: "flex",
        fontSize: 20,
        height: 42,
    },
    root: {
        flex: 1,
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",
        margin: 10,
      },
      smallCard: {
        flex: 1,
        minHeight: 100,
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        backgroundColor: "white",
        borderColor: "#E5E5E5",
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        margin: 10,
      },
    
      title: {
        fontSize: 22,
        fontWeight: "300",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      },
      resturentTitle: {
        fontSize: 16,
        fontWeight: "600",
        marginTop: 4,
        marginBottom: 4,
        display: "flex",
        color: "#565555",
      },
      foodDescription: {
        fontSize: 16,
        fontWeight: "300",
        display: "flex",
        color: "#565555",
      },
      price: {
        fontSize: 18,
        fontWeight: "600",
        display: "flex",
        color: "#EA5656",
        alignSelf: "center",
      },
      foodImageSmall: {
        borderRadius: 10,
        height: 99,
        width: 99,
        justifyContent: "flex-start",
        alignItems: "center",
        backgroundColor: "#581845",
        alignSelf: "center",
      },
      rating: {
        alignSelf: "flex-start",
      },
      productInfo: {
        flex: 9,
        justifyContent: "space-around",
      },
    
      priceView: {
        flex: 3,
        justifyContent: "space-around",
        alignItems: "flex-start",
      },
      countView: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        flex: 8,
      },
      badge: {
        backgroundColor: "green",
      },
      //Button
      btnAddRemove: {
        borderColor: "#f15b5d",
        borderRadius: 5,
        borderWidth: 0.5,
      },
      btnTitleStyle: {
        color: "#f15b5d",
      },
});

SearchScreen.navigationOptions = () => {
    return {
        header: null,
    };
};

export default SearchScreen;
