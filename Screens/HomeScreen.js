import React, { useContext, useEffect, useState, useCallback } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Dimensions
} from 'react-native';

//import CookieManager from '@react-native-community/cookies';

const { height, width } = Dimensions.get('window');
import { Context as Mycontext } from '../DataStore/AccessContext';

import MySwipper from '../components/MySwipper'
import Foods from '../components/RenderFoods'
import Categories from '../components/RenderCategories'
import { TouchableOpacity } from 'react-native-gesture-handler';


const HomeScreen = ({ navigation }) => {

    const { state, fetchCategories, fetchFoodList, fetchBanners } = useContext(Mycontext);

    const { dataCategories, dataFoods, banners } = state;

    useEffect(() => {

        fetchCategories();
        fetchFoodList();
        fetchBanners();

    }, []);

    // const unsubscribe = navigation.addListener('focus', () => {
    //     setDaloaded(true);
    // });

    const itemSelect = (item) => {

        // navigation.navigate('Detail', {
        //     Dish_Id: item.MenuDishId,
        // });
    };

    return (
        <ScrollView style={styles.container}>
            <View style={styles.sliderContainer}>
                <MySwipper bannerdata={banners} />
            </View>
            <View style={[styles.categoryContainer, { marginTop: 10 }]}>
                <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>

                    <Categories dataCategories={dataCategories} />

                </ScrollView>
            </View>
            {/* <View>
                <TouchableOpacity onPress={() => {
                    navigation.navigate('Detail', {
                        Dish_Id: 'E99223383CF2414AB0E27654BD2CC3F6'
                    });
                }}>
                    <Text>details screen</Text>
                </TouchableOpacity>
            </View> */}
            <View style={styles.cardsWrapper}>
                <Text
                    style={{
                        alignSelf: 'center',
                        fontSize: 18,
                        fontWeight: 'bold',
                        color: '#333',
                    }}>
                    Recently Viewed
        </Text>
                {
                    dataFoods !== undefined ?
                        <Foods
                            dataFoods={dataFoods}
                            onTapitem={itemSelect}
                        />
                        :
                        (<Text style={{
                            alignSelf: 'center',
                            fontSize: 12,
                            fontWeight: 'bold',
                            color: '#333',
                        }}>no data</Text>)
                }


            </View>
        </ScrollView>
    )
}


export default HomeScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    sliderContainer: {
        height: 200,
        width: '90%',
        marginTop: 10,
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 8,
    },

    wrapper: {},
    bottomTab: {
        height: 60,
        width: width,

        flexDirection: 'row',
        justifyContent: 'space-between',
        elevation: 8,
        shadowOpacity: 0.3,
        shadowRadius: 50,
    },
    itemTab: {
        width: width / 3,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },

    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        borderRadius: 8,
    },
    sliderImage: {
        height: '100%',
        width: '100%',
        alignSelf: 'center',
        borderRadius: 8,
    },
    categoryContainer: {
        flexDirection: 'row',
        width: '90%',
        alignSelf: 'center',
        marginTop: 25,
        marginBottom: 10,
    },


    cardsWrapper: {
        marginTop: 20,
        width: '90%',
        alignSelf: 'center',
    },
    card: {
        height: 100,
        marginVertical: 10,
        flexDirection: 'row',
        shadowColor: '#999',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
    },
    cardImgWrapper: {
        flex: 1,
    },
    cardImg: {
        height: '100%',
        width: '100%',
        alignSelf: 'center',
        borderRadius: 8,
        borderBottomRightRadius: 0,
        borderTopRightRadius: 0,
    },
    cardInfo: {
        flex: 2,
        padding: 10,
        borderColor: '#ccc',
        borderWidth: 1,
        borderLeftWidth: 0,
        borderBottomRightRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: '#fff',
    },
    cardTitle: {
        fontWeight: 'bold',
    },
    cardDetails: {
        fontSize: 12,
        color: '#444',
    },

});
