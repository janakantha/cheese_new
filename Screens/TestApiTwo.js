import React, { useState,useEffect } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import axios from 'axios';


const TestApiTwo = () => {

    const [menudata, setMenuData] = useState('');

useEffect(()=>{
    getMenuData();
},[])

function getMenuData() {

   

    try {
        var config = {
            method: 'get',
            url: `http://nellicafe.com/administrator/index.cfm?p=dishmenu/Menu&method=list`,

        };

        axios(config).then(response => {
            console.log(response.data)
            setMenuData(JSON.stringify(response.data));
        });
    } catch (error) {
        console.log(error);
    }
}

    return (
        <View>
            <Text>{menudata}</Text>
        </View>
    )
}

export default TestApiTwo
