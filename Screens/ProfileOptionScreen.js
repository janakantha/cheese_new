import React from 'react'
import { View, Text, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Input } from "react-native-elements";


const ProfileOptionScreen = ({ navigation }) => {
    return (
        <View>
            <View style={styles.titleView}>
                <Image resizeMode="contain" style={{ height: 70, width: 70 }} source={require('../assets/settings.png')} />
            </View>
            <View style={styles.smallCard}>
                <View style={styles.productInfo}>
                    <Text style={styles.title}>Change Account Password</Text>
                    <Text style={styles.resturentTitle}>
                        Change App login Password
                </Text>
                    <Text style={styles.foodDescription}></Text>
                </View>
                <View style={styles.priceView}>
                    <TouchableOpacity onPress={()=>{
                        navigation.navigate('Change')}
                        }>
                        <Image style={{ height: 50, width: 50 }} resizeMode="contain" source={require('../assets/next.png')} />
                    </TouchableOpacity>

                </View>
            </View>
            <View style={styles.smallCard}>
                <View style={styles.productInfo}>
                    <Text style={styles.title}>Change Profile Picture</Text>
                    <Text style={styles.resturentTitle}>
                        Change App Profile Picture
                </Text>
                    <Text style={styles.foodDescription}></Text>
                </View>
                <View style={styles.priceView}>
                    <Image style={{ height: 50, width: 50 }} resizeMode="contain" source={require('../assets/next.png')} />
                </View>
            </View>

            <View style={styles.smallCard}>
                <View style={styles.productInfo}>
                    <Text style={styles.title}>Option 1</Text>
                    <Text style={styles.resturentTitle}>
                        Option Info...
                </Text>
                    <Text style={styles.foodDescription}></Text>
                </View>
                <View style={styles.priceView}>
                    <Image style={{ height: 50, width: 50 }} resizeMode="contain" source={require('../assets/next.png')} />
                </View>
            </View>

        </View>
    )
}

export default ProfileOptionScreen

const styles = StyleSheet.create({
    smallCard: {
        flex: 1,
        minHeight: 100,
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        backgroundColor: "white",
        borderColor: "#E5E5E5",
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        margin: 10,
    },

    title: {
        fontSize: 22,
        fontWeight: "300",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    resturentTitle: {
        fontSize: 16,
        fontWeight: "600",
        marginTop: 4,
        marginBottom: 4,
        display: "flex",
        color: "#565555",
    },
    foodDescription: {
        fontSize: 16,
        fontWeight: "300",
        display: "flex",
        color: "#565555",
    },
    price: {
        fontSize: 18,
        fontWeight: "600",
        display: "flex",
        color: "#EA5656",
        alignSelf: "center",
    },
    foodImageSmall: {
        borderRadius: 10,
        height: 99,
        width: 99,
        justifyContent: "flex-start",
        alignItems: "center",
        backgroundColor: "#581845",
        alignSelf: "center",
    },
    rating: {
        alignSelf: "flex-start",
    },
    productInfo: {
        flex: 9,
        justifyContent: "space-around",
    },

    priceView: {
        flex: 3,
        justifyContent: "space-around",
        alignItems: "flex-start",
    },
    countView: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        flex: 8,
    },
    badge: {
        backgroundColor: "green",
    },
    //Button
    btnAddRemove: {
        borderColor: "#f15b5d",
        borderRadius: 5,
        borderWidth: 0.5,
    },
    btnTitleStyle: {
        color: "#f15b5d",
    },
    titleView: {

        alignSelf: 'center',
        marginTop: 35
    },
})