import React, { useContext } from "react";
import { View, StyleSheet, Image, Text, TouchableOpacity } from "react-native";
import { SafeAreaView } from "react-navigation";


const PaymentScreen = ({ navigation }) => {
   

    return (
        <SafeAreaView style={styles.contentView} forceInset={{ top: "always" }}>
            <View style={styles.titleView}>
                <Text style={{fontSize:24}}> Confirm Your Payment</Text>
                <TouchableOpacity style={{ alignItems: "center" }}>
                    <Image source={require('../assets/card_payment.png')} style={styles.imgIcon} />
                </TouchableOpacity>
            </View>
        
            <View style={styles.smallCard}>
                    <View style={styles.productInfo}>
                        <Text style={styles.title}>Chicken fried Rice</Text>
                        <Text style={styles.resturentTitle}>
                            Food Category
                        </Text>
                        <Text style={styles.foodDescription}>ipsu,,,m </Text>
                    </View>
                    <View style={styles.priceView}>
                        <Text style={styles.price}>LKR 455</Text>
                        <View style={styles.countView}>
                            
                        </View>
                    </View>

                
            </View>
            <View style={styles.smallCard}>
                    <View style={styles.productInfo}>
                        <Text style={styles.title}>Milkshake n Choco</Text>
                        <Text style={styles.resturentTitle}>
                            Food Category
                        </Text>
                        <Text style={styles.foodDescription}>ipsu,,,m </Text>
                    </View>
                    <View style={styles.priceView}>
                        <Text style={styles.price}>LKR 455</Text>
                        <View style={styles.countView}>
                            
                        </View>
                    </View>

                
            </View>
            <View style={styles.bottomView}>
                <View style={styles.amountDetails}>
                    <Text style={{ fontSize: 18 }}> Total</Text>
                    <Text style={{ fontSize: 18, fontWeight: "600" }}> $200.00</Text>
                </View>
                <TouchableOpacity onPress={() => {
                        navigation.navigate('Payment');
                    }} style={{
                        width: 300,
                        height: 40,
                        backgroundColor: "#7C1FE0",
                        alignSelf: "center",
                        borderRadius: 30,
                    }}>
                        <Text style={{
                            alignSelf: 'center',
                            fontSize: 20,
                            fontWeight: "600",
                            color: "#fff",
                            marginTop: 5
                        }}>Pay Now</Text>
                    </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    contentView: {
        backgroundColor: "#F2F2F2",
        flex: 1,
        justifyContent: "space-between",
    },
    titleView: {
        flex: 1,
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "row",
        paddingLeft: 20,
        paddingRight: 20,
    },
    listView: {
        flex: 9,
    },
    bottomView: {
        flex: 2,
    },

    imgIcon: {
        width: 60,
        height: 60,
    },
    searchOptions: {
        display: "flex",
        height: 60,
        justifyContent: "space-around",
        flexDirection: "row",
        alignItems: "center",
        marginLeft: 10,
    },
    topCategory: {
        height: 100,
        backgroundColor: "green",
    },

    amountDetails: {
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 10,
        margin: 5,
    },
    root: {
        flex: 1,
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",
        margin: 10,
      },
      smallCard: {
        flex: 1,
        minHeight: 100,
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        backgroundColor: "white",
        borderColor: "#E5E5E5",
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        margin: 10,
      },
    
      title: {
        fontSize: 22,
        fontWeight: "300",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      },
      resturentTitle: {
        fontSize: 16,
        fontWeight: "600",
        marginTop: 4,
        marginBottom: 4,
        display: "flex",
        color: "#565555",
      },
      foodDescription: {
        fontSize: 16,
        fontWeight: "300",
        display: "flex",
        color: "#565555",
      },
      price: {
        fontSize: 18,
        fontWeight: "600",
        display: "flex",
        color: "#EA5656",
        alignSelf: "center",
      },
      foodImageSmall: {
        borderRadius: 10,
        height: 99,
        width: 99,
        justifyContent: "flex-start",
        alignItems: "center",
        backgroundColor: "#581845",
        alignSelf: "center",
      },
      rating: {
        alignSelf: "flex-start",
      },
      productInfo: {
        flex: 9,
        justifyContent: "space-around",
      },
    
      priceView: {
        flex: 3,
        justifyContent: "space-around",
        alignItems: "flex-start",
      },
      countView: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        flex: 8,
      },
      badge: {
        backgroundColor: "green",
      },
      //Button
      btnAddRemove: {
        borderColor: "#f15b5d",
        borderRadius: 5,
        borderWidth: 0.5,
      },
      btnTitleStyle: {
        color: "#f15b5d",
      },
});

PaymentScreen.navigationOptions = () => {
    return {
        header: null,
    };
};

export default PaymentScreen;
