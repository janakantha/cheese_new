import React from 'react'
import { View, Text, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Input } from "react-native-elements";

const VerifyScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.contentView} forceInset={{ top: 'always' }}>

            <View style={styles.titleView}>

                <Image resizeMode="contain" style={{ height: 70, width: 70 }} source={require('../assets/verify.png')} />
            </View>
            <View style={styles.listView}>

                <View>
                    <View style={{ width: 350, alignSelf: 'center' }}>
                       
                        <Input
                            placeholder="Enter Verification Code"
                            autoCapitalize={false}
                            autoCorrect={false}
                        />
                       
                    </View>

                    <TouchableOpacity onPress={()=>{navigation.navigate('Reset')}} style={{
                        width: 300,
                        height: 40,
                        backgroundColor: "#f15b5d",
                        alignSelf: "center",
                        borderRadius: 30,
                    }}>
                        <Text style={{
                            alignSelf: 'center',
                            fontSize: 20,
                            fontWeight: "600",
                            color: "#fff",
                            marginTop: 5
                        }}>Confirm Verification Code</Text>
                    </TouchableOpacity>

                </View>
            </View>

        </SafeAreaView>
    )
}

export default VerifyScreen

const styles = StyleSheet.create({
    contentView: {
        backgroundColor: '#F2F2F2',
        flex: 1,
        justifyContent: 'space-between',
    },
    titleView: {
        flex: 1,
        alignSelf: 'center',
        marginTop: 35
    },
    listView: {
        paddingTop: 15,
        flex: 6,
    },
    bottomView: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingBottom: 20,
    },
    txtInputView: {
        marginTop: 10,
        marginBottom: 10,
    },
    spacer: {
        margin: 10,
        marginTop: 20,
        marginBottom: 20,
    },
    titleStyle: {
        fontSize: 18,
        fontWeight: "400",
        color: "#f15b5d",
    },
});