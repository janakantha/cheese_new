import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';

import Home from '../Screens/HomeScreen'
import Detail from '../Screens/DetailScreen'
import Profile from '../Screens/ProfileScreen'
import Cart from '../Screens/CartScreen'
import Order from '../Screens/OrderScreen'
import Payment from '../Screens/PaymentScreen'
import Search from '../Screens/SearchScreen'
import Login from '../Screens/LoginScreen'
import SignUp from '../Screens/SignupScreen'
import Forgot from '../Screens/ForgotPWScreen'
import Option from '../Screens/ProfileOptionScreen'
import Verify from '../Screens/VerifyScreen'
import Reset from '../Screens/ResetScreen'
import Change from '../Screens/ChangePwScreen'
import Test from '../Screens/TestApi'
import Test2 from '../Screens/TestApiTwo'



const Stack = createStackNavigator();

const HomeNavigator = (props) => {
    return (

        <Stack.Navigator initialRouteName='Home'>
            <Stack.Screen
                name='Home'
                component={Home}
                options={{
                    headerRight: (navigation) => (

                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={() => {
                                    props.navigation.navigate('Test');
                                }}><Image
                                    style={{ height: 30 }}
                                    source={require('../assets/search.png')}
                                    resizeMode="contain" />
                                    </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => {
                                    props.navigation.navigate('Profile');
                                }}
                                style={{ marginHorizontal: 10 }}>
                                <Image
                                    style={{ height: 30 }}
                                    source={require('../assets/male_user.png')}
                                    resizeMode="contain" />
                            </TouchableOpacity>
                        </View>
                    )
                }} />
            <Stack.Screen
                name='Detail'
                component={Detail}
            />
            <Stack.Screen
                name='Search'
                component={Search}
            />
            <Stack.Screen
                name='Profile'
                component={Profile}
            />
            <Stack.Screen
                name='Test'
                component={Test}
            />
            <Stack.Screen
                name='Test2'
                component={Test2}
            />
        </Stack.Navigator>

    )
}

export { HomeNavigator }

const CartNavigator = () => {
    return (

        <Stack.Navigator initialRouteName='Cart'>

            <Stack.Screen
                name='Cart'
                component={Cart}
            />
            <Stack.Screen
                name='Detail'
                component={Detail}
            />
            <Stack.Screen
                name='Profile'
                component={Profile}
            />
            <Stack.Screen
                name='Payment'
                component={Payment}
            />
            <Stack.Screen
                name='Order'
                component={Order}
            />
        </Stack.Navigator>

    )
}

export { CartNavigator }

const ProfileNavigator = () => {
    return (

        <Stack.Navigator initialRouteName='Profile'>

            <Stack.Screen
                name='Detail'
                component={Detail}
            />
            <Stack.Screen
                name='Profile'
                component={Profile}
            />
             <Stack.Screen
                name='Login'
                component={Login}
            />
             <Stack.Screen
                name='SignUp'
                component={SignUp}
            />
             <Stack.Screen
                name='Forgot'
                component={Forgot}
            />
             <Stack.Screen
                name='Option'
                component={Option}
            />
            <Stack.Screen
                name='Verify'
                component={Verify}
            />
             <Stack.Screen
                name='Reset'
                component={Reset}
            />
             <Stack.Screen
                name='Change'
                component={Change}
            />
        </Stack.Navigator>

    )
}

export { ProfileNavigator } 
