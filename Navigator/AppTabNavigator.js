import * as React from 'react';
import { Image, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import { CartNavigator, ProfileNavigator, HomeNavigator } from '../Navigator/AppNavigator'

import Home from '../Screens/HomeScreen';
import Detail from '../Screens/DetailScreen';
import Profile from '../Screens/ProfileScreen';
import Cart from '../Screens/CartScreen'

const Tab = createMaterialBottomTabNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <Tab.Navigator
                initialRouteName="Home"
                activeColor="#f0edf6"
                inactiveColor="#694fad"
                barStyle={{ backgroundColor: '#694fad' }}
            >
                <Tab.Screen name="Home" component={HomeNavigator}
                    options={{
                        tabBarLabel: 'Home',
                        tabBarIcon: ({ color }) => (
                            <Image style={{ height: 28 }} source={require('../assets/home.png')} resizeMode="contain" />
                        ),
                    }} />
                <Tab.Screen name="Cart" component={CartNavigator}
                    options={{
                        tabBarLabel: 'Cart',
                        tabBarIcon: ({ color }) => (
                            <Image style={{ height: 28 }} source={require('../assets/shopping_cart.png')} resizeMode="contain" />
                        ),
                    }} />
                <Tab.Screen name="Profile" component={ProfileNavigator}
                    options={{
                        tabBarLabel: 'Profile',
                        tabBarIcon: ({ color }) => (
                            <Image style={{ height: 28 }} source={require('../assets/male_user_1.png')} resizeMode="contain" />
                        ),
                    }} />

            </Tab.Navigator>
        </NavigationContainer>
    );
}
