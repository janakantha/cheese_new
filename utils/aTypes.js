const Action = {
  ERROR: "Error",

  ALL_FOODS: "all_foods",
  ALL_CATEGORIES: "all_categories",
  GET_BANNERS: "get_banners",
  SET_USERDATA :"set_userdata",
  GET_USERDATA:"get_userdata",
  GET_MENUDETAILS:"get_menudetails"
};

export default Action;
