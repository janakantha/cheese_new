import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native'

const Category = (
    item,
) => {

   // console.log(JSON.stringify(item.item.item));

    // function renderc() {
    //     console.log(dataCategories.length % 3 == 0 ? 't' : 'f');

    //     let categoryViewList = [];

    //     let rawcell = 0;
    //     let rawFilled = false;
    //     let raw = 1;


    //     for (i = 0; i < dataCategories.length; i++) {

    //         rawcell++;

    //         if (i + 1 % 3 == 0) {
    //             rawFilled = true;
    //             raw++;
    //         }

    //         // let tmpitem = (
    //         //     <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
    //         //         <Text style={[styles.text, { color: '#fff', }]}>
    //         //             {item.CartItems[i].menu_dish_name} #{item.CartItems[i].qty}
    //         //         </Text>
    //         //         <Text style={[styles.text, { color: '#fff', }]}>LKR {item.CartItems[i].dish_price}</Text>

    //         //     </View>

    //         // );

    //         // categoryViewList[i] = (tmpitem);
    //     }


    return (
        <TouchableOpacity style={styles.categoryBtn} onPress={() => { }}>
            <View style={styles.categoryIcon}>
                <Image style={{ height: 40 }}
                    resizeMode="contain"
                    source={require('../assets/food.png')} />
            </View>
            <Text style={styles.categoryBtnTxt}>{item.item.item.MenuName}</Text>
        </TouchableOpacity>
    )
}

export default Category

const styles = StyleSheet.create({
    categoryBtn: {
        flex: 1,
        width: '30%',
        marginHorizontal: 17,
        alignSelf: 'center',
    },
    categoryIcon: {
        borderWidth: 0,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        width: 70,
        height: 70,
        backgroundColor: '#fdeae7' /* '#FF6347' */,
        borderRadius: 50,
    },
    categoryBtnTxt: {
        alignSelf: 'center',
        marginTop: 5,
        color: '#de4f35',
    },
});
