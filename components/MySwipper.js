import React from 'react'
import { View, Image, StyleSheet } from 'react-native'
import Swiper from 'react-native-swiper';

const MySwipper = ({ bannerdata }) => {
  
    return (

        <Swiper
            autoplay={true}
            horizontal={true}
            height={200}
            activeDotColor="#FF6347">
            {

                bannerdata !== undefined ?

                    bannerdata.map((item) => {//without Map didnt worked

                        return (
                            <View style={styles.slide}>
                                <Image

                                    source={{ uri: item }}
                                    resizeMode="contain"
                                    style={styles.sliderImage}
                                />
                            </View>

                        )
                    }) : (
                        <View style={styles.slide}>
                            <Image
                                source={require('../assets/logo.png')}
                                resizeMode="contain"
                                style={styles.sliderImage}
                            />
                        </View>)
            }
        </Swiper>

    )
}

export default MySwipper;

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        borderRadius: 8,
    },
    sliderImage: {
        height: '100%',
        width: '100%',
        alignSelf: 'center',
        borderRadius: 8,
    },

});
