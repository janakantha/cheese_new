import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

const CardView = (title, category, info, price) => {
    return (
        <View style={styles.smallCard}>
            <View style={styles.productInfo}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.resturentTitle}>
                    {category}
                </Text>
                <Text style={styles.foodDescription}>{info}</Text>
            </View>
            <View style={styles.priceView}>
                <Text style={styles.price}>LKR {price}</Text>
                <View style={styles.countView}>
                    <TouchableOpacity style={{
                        width: 30,
                        height: 40,
                        backgroundColor: "#f15b5d",
                        alignSelf: "center",
                        borderRadius: 30,
                    }}>
                        <Text style={{
                            alignSelf: 'center',
                            fontSize: 20,
                            fontWeight: "600",
                            color: "#fff",
                            marginTop: 5
                        }}>-</Text>
                    </TouchableOpacity>

                    <Text
                        h4
                        style={{ alignSelf: "center", margin: 5, fontWeight: "600" }}
                    >
                        #1
                            </Text>
                    <TouchableOpacity style={{
                        width: 30,
                        height: 40,
                        backgroundColor: "#f15b5d",
                        alignSelf: "center",
                        borderRadius: 30,
                    }}>
                        <Text style={{
                            alignSelf: 'center',
                            fontSize: 20,
                            fontWeight: "600",
                            color: "#fff",
                            marginTop: 5
                        }}>+</Text>
                    </TouchableOpacity>

                </View>
            </View>

        </View>
    )
}

export default CardView

const styles = StyleSheet.create({
    smallCard: {
        flex: 1,
        minHeight: 100,
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        backgroundColor: "white",
        borderColor: "#E5E5E5",
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        margin: 10,
    },

    title: {
        fontSize: 22,
        fontWeight: "300",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    resturentTitle: {
        fontSize: 16,
        fontWeight: "600",
        marginTop: 4,
        marginBottom: 4,
        display: "flex",
        color: "#565555",
    },
    foodDescription: {
        fontSize: 16,
        fontWeight: "300",
        display: "flex",
        color: "#565555",
    },
    price: {
        fontSize: 18,
        fontWeight: "600",
        display: "flex",
        color: "#EA5656",
        alignSelf: "center",
    },
    foodImageSmall: {
        borderRadius: 10,
        height: 99,
        width: 99,
        justifyContent: "flex-start",
        alignItems: "center",
        backgroundColor: "#581845",
        alignSelf: "center",
    },
    rating: {
        alignSelf: "flex-start",
    },
    productInfo: {
        flex: 9,
        justifyContent: "space-around",
    },

    priceView: {
        flex: 3,
        justifyContent: "space-around",
        alignItems: "flex-start",
    },
    countView: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        flex: 8,
    },
    badge: {
        backgroundColor: "green",
    },
    //Button
    btnAddRemove: {
        borderColor: "#f15b5d",
        borderRadius: 5,
        borderWidth: 0.5,
    },
    btnTitleStyle: {
        color: "#f15b5d",
    },
})