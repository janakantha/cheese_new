import React from "react";
import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    Dimensions,
    Text
} from "react-native";



const deviceWidth = Math.round(Dimensions.get("window").width);

const OrderCard = () => {
   
    let isAdded = false;
    let currentQty = 1;

    const smallCard = () => {
        return (
            <TouchableOpacity
                style={smallStyles.smallCard}
               
            >
                <Image style={smallStyles.foodImageSmall} source={require('../assets/Fast_Cart.png')} />
                <View style={smallStyles.productInfo}>
                    <Text style={smallStyles.title}>name</Text>
                    <Text style={smallStyles.resturentTitle}>
                        Category | Not delivered
                    </Text>
                    
                </View>
                <View style={smallStyles.shopView}>
                    <Text style={smallStyles.productSize}>LkR 1234</Text>
                    

                
                </View>
            </TouchableOpacity>
        );
    };

    
    return smallCard();
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",
        margin: 10,
    },
    foodImage: {
        borderRadius: 20,
        height: 220,
        width: deviceWidth - 30,
        backgroundColor: "red",
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center",
        backgroundColor: "#581845",
    },
    title: {
        fontSize: 14,
        fontWeight: "500",
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        marginTop: 10,
        color: "#636363",
    },
    countView: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        flex: 8,
    },
});

const smallStyles = StyleSheet.create({
    smallCard: {
        flex: 1,
        height: 100,
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        backgroundColor: "white",
        borderColor: "#E5E5E5",
        borderWidth: 1,
        borderRadius: 10,
        margin: 10,
    },

    title: {
        fontSize: 16,
        fontWeight: "500",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    resturentTitle: {
        fontSize: 16,
        display: "flex",
        color: "#565555",
    },
    price: {
        fontSize: 18,
        fontWeight: "400",
        display: "flex",
        color: "#EA5656",
    },
    foodImageSmall: {
        borderRadius: 10,
        height: 69,
        width: 69,
        justifyContent: "flex-start",
        alignItems: "center",
        backgroundColor: "#581845",
        alignSelf: "center",
    },
    rating: {
        alignSelf: "flex-start",
    },
    productInfo: {
        flex: 1,
        padding: 5,
        justifyContent: "space-around",
    },
    shopView: {
        justifyContent: "space-around",
        padding: 10,
        alignItems: "center",
    },
    productSize: {
        fontSize: 20,
        fontWeight: "600",
        color: "#848484",
    },
});

export default OrderCard;
