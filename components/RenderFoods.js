import React from 'react'
import { View, Text, FlatList, StyleSheet, TouchableOpacity, Image } from 'react-native'

//import Fooditem from './FoodItem'

function RenderFoods(dataFoods,onTapitem){

    let imagesUrls = [
        'https://d1bv4heaa2n05k.cloudfront.net/user-images/1533718585763/shutterstock-1118383397_destinationMain_1533718754411.jpeg',
        'https://www.musselmans.com/wp-content/uploads/0cc89014-004d-4557-b58a-c85fb83d89fb_Landscape-580x435.jpg',
        'https://npr.brightspotcdn.com/dims4/default/095a267/2147483647/strip/true/crop/960x718+0+0/resize/880x658!/quality/90/?url=http%3A%2F%2Fnpr-brightspot.s3.amazonaws.com%2Flegacy%2Fsites%2Fkcur%2Ffiles%2F201804%2F27067712_10155356717641819_7145104953971378878_n.jpg',
        'https://www.tasteofhome.com/wp-content/uploads/2018/01/Marinated-Cheese_EXPS_THCA18_41873_B01_23_3b-696x696.jpg',
        'https://c.ndtvimg.com/2020-01/dd46j918_chilli-chicken_625x300_21_January_20.jpg',
        'https://www.tasteofhome.com/wp-content/uploads/2018/01/Tender-Salsa-Beef_EXPS_SCBZ18_128014_B07_11_8b-1-696x696.jpg',
        'https://food.fnr.sndimg.com/content/dam/images/food/fullset/2018/4/1/1/LS-Library_Sweet-Sour-Glazed-Shrimp_s4x3.jpg.rend.hgtvcom.616.462.suffix/1522651448204.jpeg',
        'https://food.fnr.sndimg.com/content/dam/images/food/fullset/2008/3/26/0/IE0309_French-Toast.jpg.rend.hgtvcom.616.462.suffix/1431730431340.jpeg',
        'https://cdn.tasteatlas.com/images/toplistarticles/d0e6a0a79d5f4197a51f4ca065393ffe.jpg?w=600&h=450',
        'https://yupitsvegan.com/wp-content/uploads/2018/09/sesame-garlic-noodles-vegan-gluten-free-square-500x375.jpg'
    ];


    const onItemSelect = (item) => {
        onTapitem(item);
      };



    function renderitems(oneitem) {
       
        return (
            <TouchableOpacity style={styles.card}
                onPress={(oneitem)=>onItemSelect(oneitem.item) }>
                <View style={styles.cardImgWrapper}>
                    <Image
                        source={{ uri: imagesUrls[oneitem.item.rownumber - 1] }}
                        resizeMode="contain"
                        style={styles.cardImg}
                    />
                </View>
                <View style={styles.cardInfo}>
                    <Text style={styles.cardTitle}>{oneitem.item.MenuDishName}</Text>
                    <Text style={styles.cardDetails}>Price : LKR {oneitem.item.Price}</Text>
                    <Text numberOfLines={3} style={styles.cardDetails}>

                        {oneitem.item.description.length < 35
                            ? `${oneitem.item.description}`
                            : `${oneitem.item.description.substring(0, 72)}...`}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <View>
            <FlatList
                data={dataFoods.dataFoods}
                keyExtractor={(item) => (item.rownumber)}
                renderItem={(oneitem) => renderitems(oneitem)}
            />
        </View>
    )
}

export default RenderFoods

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    sliderContainer: {
        height: 200,
        width: '90%',
        marginTop: 10,
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 8,
    },

    wrapper: {},

    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        borderRadius: 8,
    },
    sliderImage: {
        height: '100%',
        width: '100%',
        alignSelf: 'center',
        borderRadius: 8,
    },
    categoryContainer: {
        flexDirection: 'row',
        width: '90%',
        alignSelf: 'center',
        marginTop: 25,
        marginBottom: 10,
    },
    categoryBtn: {
        flex: 1,
        width: '30%',
        marginHorizontal: 0,
        alignSelf: 'center',
    },
    categoryIcon: {
        borderWidth: 0,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        width: 70,
        height: 70,
        backgroundColor: '#fdeae7' /* '#FF6347' */,
        borderRadius: 50,
    },
    categoryBtnTxt: {
        alignSelf: 'center',
        marginTop: 5,
        color: '#de4f35',
    },
    cardsWrapper: {
        marginTop: 20,
        width: '90%',
        alignSelf: 'center',
    },
    card: {
        height: 100,
        marginVertical: 10,
        flexDirection: 'row',
        shadowColor: '#999',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
    },
    cardImgWrapper: {
        flex: 1,
    },
    cardImg: {
        height: '100%',
        width: '100%',
        alignSelf: 'center',
        borderRadius: 8,
        borderBottomRightRadius: 0,
        borderTopRightRadius: 0,
    },
    cardInfo: {
        flex: 2,
        padding: 10,
        borderColor: '#ccc',
        borderWidth: 1,
        borderLeftWidth: 0,
        borderBottomRightRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: '#fff',
    },
    cardTitle: {
        fontWeight: 'bold',
    },
    cardDetails: {
        fontSize: 12,
        color: '#444',
    },
});
