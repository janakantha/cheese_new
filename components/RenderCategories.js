import React from 'react'
import { View, Text, FlatList } from 'react-native'

import Category from './Category'

const RenderCategories = ({
    dataCategories,
}) => {

    return (
        <View>
            <FlatList
                horizontal={true}
                data={dataCategories}
                renderItem={(item) => (
                    <Category item={item} />)}
            />
        </View>
    )

}

export default RenderCategories
