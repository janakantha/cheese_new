
import React from 'react';
import { Provider as MyProvider } from './DataStore/AccessContext';
import Stack_Navigator from './Navigator/AppNavigator'
import Tab_Navigator from './Navigator/AppTabNavigator'

export default function App() {
  return (
    <MyProvider>
      <Tab_Navigator/>
    </MyProvider>
  );
}